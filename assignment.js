document.getElementById("hello_text").textContent = "はじめてのJavaScript";
document.addEventListener("keydown",onKeyDown);

function onKeyDown(Event){
  if (event.keyCode === 37) {
    moveLeft();
 }else if(event.keyCode === 39){
    moveRight();
 }
}
var count = 0;

var cells;

var blocks={
  a:{
    class:"a",
    pattern:[
      [1,1,1,1]
    ]
  },
  b:{
    class:"b",
    pattern:[
      [1,0,1]
    ]
  },
  c:{
    class:"c",
    pattern:[
      [1,1,0]
    ]
  },
}


loadTable();
setInterval(function() { // 何回目かを数えるために変数countを1ずつ増やす
  count++;
  document.getElementById("hello_text").textContent = "はじめてのJavaScript(" + count + ")"; // 何回目かを文字にまとめて表示する

  for (var row = 0; row &lt; 2; row++) {
     for (var col = 0; col &lt; 10; col++) {
       if (cells[row][col].className !== "") {
         alert("game over");
       }
     }
   }

  if(hasFallingBlock()){
  fallBlocks();
}else{
  deleteRow();
  generateBlock();
}
}, 1000);

function loadTable(){
 var td_array = document.getElementsByTagName("td");
 var cells = [];
 var index = 0;
 for (var row = 0; row < 20; row++) {
   cells[row] = []; // 配列のそれぞれの要素を配列にする(2次元配列にする)
   for (var col = 0; col < 10; col++) {
     cells[row][col] = td_array[index];
     index++;
   }
 }

}

function fallBlocks(){
 for (var i = 0; i < 10; i++) {
     if(cells[19][i].className = ""){
     isFalling=false;
     return ;
   }
 }
   // 下から二番目の行から繰り返しクラスを下げていく
   for (var row = 18; row >= 0; row--) {
     for (var col = 0; col < 10; col++) {
       if (cells[row][col].className !== "") {
         cells[row + 1][col].className = cells[row][col].className;
         cells[row][col].className = "";
       }
     }
   }
 }

 var isFalling = true;
function hasFallingBlock(){
 //落下中のブロックを確認する
 return true;
}

function deleteRow(){
  for(var row = 19;row & gt;=0; row--){
    for(var col = 0;col & lt;10;col++){
      if (cells[row][col].className === "") {
        canDelete = false;
    }
  }if(canDelete){
    for(var col = 0; col &lt; 10; col++) {
        cells[row][col].className = "";
      }
  }
  for(var downRow = row - 1; row & gt;=0;row--){
    for(var col = 0;col & lt;10;col++){
      cells[downRow + 1][col].className = cells[downRow][col].className;
      cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
      cells[downRow][col].className = "";
      cells[downRow][col].blockNum = null;
    }
  }
}
}

function generateBlock(){
  //ブロックをランダムに生成する
  //1.ブロックパターンから1つ選択
  var keys=Object.keys(blocks);
  var nextBlockKey = keys[Math.floor(Math.random() * keys.length)];
  var nextBlock = blocks[nextBlockKey];
  var nextFallingBlockNum = fallingBlockNum + 1;
  //2.選択したブロックを配置
  var pattern=nextBlock.pattern;
  for (var row = 0;row < pattern.length; row++) {
    for(var col =0;col < pattern[row].length; col++){
      if (pattern[row][col]) {
        cells[row][col + 3].className = nextBlock.class;
        cells[row][col + 3].blockNum = nextFallingBlockNum;
    }
  //3.落下中のブロックがあると仮定
}

function moveRight(){
  for(var row = 0;row & lt;20;row++){
    for(var col =9;col & gt;=0;col--){
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col + 1].className = cells[row][col].className;
        cells[row][col + 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}

function moveLeft(){
  for (var row = 0; row &lt; 20; row++) {
    for (var col = 0; col &lt; 10; col++) {
      if (cells[row][col].blockNum === fallingBlockNum) {
        cells[row][col - 1].className = cells[row][col].className;
        cells[row][col - 1].blockNum = cells[row][col].blockNum;
        cells[row][col].className = "";
        cells[row][col].blockNum = null;
      }
    }
  }
}
